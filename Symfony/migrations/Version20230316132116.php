<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230316132116 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE photos (id INT AUTO_INCREMENT NOT NULL, bien_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, file VARCHAR(255) NOT NULL, INDEX IDX_876E0D9BD95B80F (bien_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, civilite VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, ville VARCHAR(255) NOT NULL, complement_adresse VARCHAR(255) NOT NULL, code_postal VARCHAR(255) NOT NULL, pays VARCHAR(255) NOT NULL, tel_fixe VARCHAR(255) NOT NULL, tel_portable VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE photos ADD CONSTRAINT FK_876E0D9BD95B80F FOREIGN KEY (bien_id) REFERENCES bien (id)');
        $this->addSql('ALTER TABLE bien ADD user_id INT NOT NULL, ADD type_transactions_id INT NOT NULL, ADD type_bien_id INT NOT NULL, ADD id_type_bien INT NOT NULL, ADD titre VARCHAR(255) NOT NULL, ADD type_de_bien INT NOT NULL, ADD type_de_trans INT NOT NULL, ADD resume VARCHAR(255) NOT NULL, ADD description LONGTEXT NOT NULL, ADD prix NUMERIC(10, 0) NOT NULL, ADD rue VARCHAR(255) NOT NULL, ADD ville VARCHAR(255) NOT NULL, ADD complement_adresse VARCHAR(255) NOT NULL, ADD code_postal VARCHAR(255) NOT NULL, ADD pays VARCHAR(255) NOT NULL, ADD longitude NUMERIC(12, 9) NOT NULL, ADD latitude NUMERIC(12, 9) NOT NULL, ADD status TINYINT(1) NOT NULL, ADD nombre_de_vues INT NOT NULL');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC386A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC38680D5F40B FOREIGN KEY (type_transactions_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC38695B4D7FA FOREIGN KEY (type_bien_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_45EDC386A76ED395 ON bien (user_id)');
        $this->addSql('CREATE INDEX IDX_45EDC38680D5F40B ON bien (type_transactions_id)');
        $this->addSql('CREATE INDEX IDX_45EDC38695B4D7FA ON bien (type_bien_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC386A76ED395');
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC38680D5F40B');
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC38695B4D7FA');
        $this->addSql('ALTER TABLE photos DROP FOREIGN KEY FK_876E0D9BD95B80F');
        $this->addSql('DROP TABLE photos');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP INDEX IDX_45EDC386A76ED395 ON bien');
        $this->addSql('DROP INDEX IDX_45EDC38680D5F40B ON bien');
        $this->addSql('DROP INDEX IDX_45EDC38695B4D7FA ON bien');
        $this->addSql('ALTER TABLE bien DROP user_id, DROP type_transactions_id, DROP type_bien_id, DROP id_type_bien, DROP titre, DROP type_de_bien, DROP type_de_trans, DROP resume, DROP description, DROP prix, DROP rue, DROP ville, DROP complement_adresse, DROP code_postal, DROP pays, DROP longitude, DROP latitude, DROP status, DROP nombre_de_vues');
    }
}
