<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230406093300 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_C4E0A61FE7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC38680D5F40B');
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC38695B4D7FA');
        $this->addSql('DROP INDEX IDX_45EDC38680D5F40B ON bien');
        $this->addSql('ALTER TABLE bien ADD type_transaction_id INT NOT NULL, ADD lieu_affiche VARCHAR(255) NOT NULL, DROP type_transactions_id, DROP id_type_bien, DROP type_de_bien, DROP type_de_trans, DROP nombre_de_vues, CHANGE resume resume LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC3867903E29B FOREIGN KEY (type_transaction_id) REFERENCES types_transaction (id)');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC38695B4D7FA FOREIGN KEY (type_bien_id) REFERENCES types_bien (id)');
        $this->addSql('CREATE INDEX IDX_45EDC3867903E29B ON bien (type_transaction_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE team');
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC3867903E29B');
        $this->addSql('ALTER TABLE bien DROP FOREIGN KEY FK_45EDC38695B4D7FA');
        $this->addSql('DROP INDEX IDX_45EDC3867903E29B ON bien');
        $this->addSql('ALTER TABLE bien ADD id_type_bien INT NOT NULL, ADD type_de_bien INT NOT NULL, ADD type_de_trans INT NOT NULL, ADD nombre_de_vues INT NOT NULL, DROP lieu_affiche, CHANGE resume resume VARCHAR(255) NOT NULL, CHANGE type_transaction_id type_transactions_id INT NOT NULL');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC38680D5F40B FOREIGN KEY (type_transactions_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE bien ADD CONSTRAINT FK_45EDC38695B4D7FA FOREIGN KEY (type_bien_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_45EDC38680D5F40B ON bien (type_transactions_id)');
    }
}
