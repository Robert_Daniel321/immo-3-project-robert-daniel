<?php

namespace App\Controller\Admin;

use App\Entity\Photos;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class PhotosCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Photos::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        
        yield    IdField::new('id')->hideOnForm();
        yield    TextField::new('Title');
        yield    ImageField::new('File')->setBasePath('/img')->setUploadDir('/public/img');
        yield   AssociationField::new('bien');
        
    }
    
}
