<?php

namespace App\Controller\Admin;

use App\Entity\Bien;
use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\Integer;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class BienCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Bien::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnForm();
          yield TextField::new('titre');
          yield TextEditorField::new('resume');
          yield TextEditorField::new('description');
          yield MoneyField::new('prix')->setCurrency('EUR');
          yield TextField::new('rue');
          yield TextField::new('ville');
          yield TextField::new('complement_adresse');
          yield TextField::new('code_postal');
          yield CountryField::new('pays');
          yield TextField::new('lieu_affiche');
          yield IntegerField::new('longitude');
          yield IntegerField::new('latitude');
          yield BooleanField::new('status');
          yield CollectionField::new('photos');
          yield AssociationField::new('type_bien');
          yield AssociationField::new('type_transaction');
          yield AssociationField::new('user');
        
    }
    
}
