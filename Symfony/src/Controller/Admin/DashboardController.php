<?php

namespace App\Controller\Admin;

use App\Entity\Bien;
use App\Entity\Team;
use App\Entity\User;
use App\Entity\Photos;
use App\Entity\TypesBien;
use App\Entity\TypesTransaction;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {

        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Immo Project Symfony');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Team', 'fas fa-list', Team::class);
        yield MenuItem::linkToCrud('Bien', 'fas fa-list', Bien::class);
        yield MenuItem::linkToCrud('Photos', 'fas fa-list', Photos::class);
        yield MenuItem::linkToCrud('User', 'fas fa-list', User::class);
        yield MenuItem::linkToCrud('Type de transaction', 'fas fa-list', TypesTransaction::class);
        yield MenuItem::linkToCrud('Type de Bien', 'fas fa-list', TypesBien::class);
    }
}
