<?php

namespace App\Controller\Admin;

use App\Entity\TypesBien;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TypesBienCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TypesBien::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
