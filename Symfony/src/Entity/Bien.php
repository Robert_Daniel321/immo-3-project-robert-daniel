<?php

namespace App\Entity;

use App\Entity\Photos;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\BienRepository;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BienRepository::class)]

#[ApiResource
(
    operations: [
        new Post(normalizationContext: ['groups' => 'bien:item']),
        new Get(normalizationContext: ['groups' => 'bien:item']),
        new GetCollection(normalizationContext: ['groups' => 'bien:list'])
    ],
    order: ['prix' => 'ASC'],
    paginationEnabled: false,
)]

class Bien
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['bien:list', 'bien:item'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['bien:list', 'bien:item'])]
    private ?string $titre = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['bien:list', 'bien:item'])]
    private ?string $resume = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['bien:list', 'bien:item'])]
    private ?string $description = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: '0')]
    #[Groups(['bien:list', 'bien:item'])]
    private ?string $prix = null;

    #[ORM\Column(length: 255)]
    private ?string $rue = null;

    #[ORM\Column(length: 255)]
    #[Groups(['bien:list', 'bien:item'])]
    private ?string $ville = null;

    #[ORM\Column(length: 255)]
    private ?string $complement_adresse = null;

    #[ORM\Column(length: 255)]
    #[Groups(['bien:list', 'bien:item'])]
    private ?string $code_postal = null;

    #[ORM\Column(length: 255)]
    private ?string $pays = null;

    #[ORM\Column(length: 255)]
    #[Groups(['bien:list', 'bien:item'])]
    private ?string $lieu_affiche = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 12, scale: 9)]
    private ?string $longitude = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 12, scale: 9)]
    private ?string $latitude = null;

    #[ORM\Column]
    private ?bool $status = null;

    #[ORM\ManyToOne(inversedBy: 'bienss')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\ManyToOne(inversedBy: 'biens')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['bien:list', 'bien:item'])]
    private ?TypesTransaction $type_transaction = null;

    #[ORM\ManyToOne(inversedBy: 'biens')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['bien:list', 'bien:item'])]
    private ?TypesBien $type_bien = null;

    #[ORM\OneToMany(mappedBy: 'bien', targetEntity: Photos::class, fetch:"EAGER")]
    #[ORM\JoinColumn(name: 'photos', referencedColumnName: 'bien')]
    #[Groups(['bien:list', 'bien:item'])]
    private Collection $photos;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getRue(): ?string
    {
        return $this->rue;
    }

    public function setRue(string $rue): self
    {
        $this->rue = $rue;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getComplementAdresse(): ?string
    {
        return $this->complement_adresse;
    }

    public function setComplementAdresse(string $complement_adresse): self
    {
        $this->complement_adresse = $complement_adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(string $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getLieuAffiche(): ?string
    {
        return $this->lieu_affiche;
    }

    public function setLieuAffiche(string $lieu_affiche): self
    {
        $this->lieu_affiche = $lieu_affiche;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function isStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTypeTransaction(): ?TypesTransaction
    {
        return $this->type_transaction;
    }

    public function setTypeTransaction(?TypesTransaction $type_transaction): self
    {
        $this->type_transaction = $type_transaction;

        return $this;
    }

    public function getTypeBien(): ?TypesBien
    {
        return $this->type_bien;
    }

    public function setTypeBien(?TypesBien $type_bien): self
    {
        $this->type_bien = $type_bien;

        return $this;
    }

    /**
     * @return Collection<int, Photos>
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photos $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos->add($photo);
            $photo->setBien($this);
        }

        return $this;
    }

    public function removePhoto(Photos $photo): self
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getBien() === $this) {
                $photo->setBien(null);
            }
        }

        return $this;
    }
    public function __toString(){
        return $this->titre;
    }
}
