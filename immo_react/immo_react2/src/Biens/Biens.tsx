import React from 'react';
import { Link } from "react-router-dom";
import Bien from './Bien/Bien';

import { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
// import './App.css'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
// import Home from './Home/Home'
// import Bien from './Biens/Bien/Bien'

// import Header from './Header/Header'
// import Biens from './Biens/Biens'

export interface Biens {



  id: number

  titre: string;
  resume: string;
  description: string;
  prix: number;
  rue: string;
  ville: string;
  complement_adresse: string;
  code_postal: string;
  pays: string;
  lieu_affiche: string;
  longitude: string;
  latitude: string;
  status: boolean;
  user: number;
  type_transaction: typeTransaction;

  type_bien: typeBien;
  photos: string;

}

type typeTransaction = {
  entitled: string;

}


type typeBien = {
  entitled: string;
}


export default function Biens() {

  const [biens, setBiens] = useState<Biens[]>([]);

  useEffect(() => {
    fetch('http://127.0.0.1:8000/api/biens')
      .then(response => response.json())
      .then(biens => setBiens(biens['hydra:member']))
      .catch(error => console.error(error));

  }, []);

  console.log(biens);

  return (
    <div className="container">
      <h1 className="header">Boxes</h1>
      <div className="card-container">
        {biens && biens.map((bien, index) =>
          <Bien bien={bien} key={index}/>
        )}


      </div>
      <style>
        {`
        .container {
          padding: 20px;
        }
        .header {
          margin-bottom: 20px;
        }
        .card-container {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
        }
        .card {
          width: calc(33.33% - 10px);
          margin-bottom: 20px;
          box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.1);
          border-radius: 5px;
        }
        .card-img-top {
          height: 200px;
          object-fit: cover;
          border-top-left-radius: 5px;
          border-top-right-radius: 5px;
        }
        .card-body {
          padding: 20px;
        }
        .card-title {
          margin-bottom: 10px;
        }
        .btn-primary {
          background-color: #007bff;
          border-color: #007bff;
        }
      `}
      </style>
    </div>
  );
}


