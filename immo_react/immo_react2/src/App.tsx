import { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Home from './Home/Home'
import Bien from './Biens/Bien/Bien'

import Header from './Header/Header'
import Biens from './Biens/Biens'



  
  function App() {


     
    return(
      <div>
        <BrowserRouter>
        <Header></Header>
        <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/biens" element={<Biens />} />
        
   
        </Routes>
        
        </BrowserRouter>
      </div>
    )
  }



export default App



// <div id="bdy">
//       <Menu />
//       <BrowserRouter>

//         <Routes>
//           <Route path="/"/>
//           <Route path="/Announce" element={<Announce biens={biens} />} />
//           {/* <Route path="/content/:filtre?"  element={<Content />} /> */}
//           <Route path="/Discription/:id" element={<Discription/>} />
//         </Routes>

//       </BrowserRouter>
//       {/* <Button txt='Submit'/> */}


//       <Footer />
//     </div>