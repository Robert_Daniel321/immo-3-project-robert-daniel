
import React from "react";
import backgroundImage from '../Photos/backgroundImage.jpg';

export default function Home() {
  const Bien = () => {
    return (
      <div className="card" style={{ margin: "10px" }}>
        <img className="card-img-top" src="" alt="" />
        <div className="card-body">
          <h5 className="card-title">Maison</h5>
          <p className="card-text">
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <a href="#" className="btn btn-primary">
            Acheter
          </a>
        </div>
      </div>
    );
  };

  return (
    <div
      style={{
        backgroundImage: `url(${backgroundImage})`,
        backgroundSize: "cover",
        minHeight: "100vh",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <div>
        <h1>Acheter et louer des maison!</h1>
        <p>This is some text about me and my interests.</p>
      </div>

      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Bien />
        <Bien />
        <Bien />
      </div>

      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Bien />
        <Bien />
        <Bien />
      </div>
    </div>
  );
}
























// import React from "react";
// import BackgroundImage from '../Photos/backgroundImage.jpg'
// import './Comp.css'


// export default function Home() {
//   const Bien = () => {
//     return (
//       <div>
//       <div className="card" >
//         <img className="card-img-top" src={BackgroundImage} alt="" />
//         <div className="card-body">
//           <h5 className="card-title">Maison</h5>
//           <p className="card-text">
//             Some quick example text to build on the card title and make up the
//             bulk of the card's content.
//           </p>
//           <a href="#" className="btn btn-primary">
//             Acheter
//           </a>
//         </div>
//       </div>
    
  

    
//     {/* // <div
//       style={{
//         backgroundImage: `url(${backgroundImage})`,
//         backgroundSize: "cover",
//         minHeight: "100vh",
//         display: "flex",
//         flexDirection: "column",
//       }}
//     > */}

//       <div className="container-fluid" id="banner"></div>
//       <div>
//         <h1>Acheter et louer des maison!</h1>
//         <p>This is some text about me and my interests.</p>
//       </div>

//       <div style={{ display: "flex", justifyContent: "space-between" }}>
//         <Bien />
//         <Bien />
//         <Bien />
//       </div>

//       <div style={{ display: "flex", justifyContent: "space-between" }}>
//         <Bien />
//         <Bien />
//         <Bien />
//       </div>
//     </div>
   
//   );
  
// }
// }







// import React from "react";
// import backgroundImage from 'immo_react/immo_react2/Photos/backgroundImage.jpg';


// export default function Home() {
//   const Bien = () => {
//     return (
//       <div className="card" style={{ margin: "10px" }}>
//         <img className="card-img-top" src="" alt="" />
//         <div className="card-body">
//           <h5 className="card-title">Maison</h5>
//           <p className="card-text">
//             Some quick example text to build on the card title and make up the
//             bulk of the card's content.
//           </p>
//           <a href="#" className="btn btn-primary">
//             Acheter
//           </a>
//         </div>
//       </div>
//     );
//   };

//   return (
//     <div>
//       <h1>Acheter et louer des maison!</h1>
//       <p>This is some text about me and my interests.</p>
//       <div className="d-flex justify-content-between">
//         <Bien />
//         <Bien />
//         <Bien />
    
//       </div>

//       <div className="d-flex justify-content-between">
//         <Bien />
//         <Bien />
//         <Bien />
     
//       </div>
//     </div>    
//   );


// }


